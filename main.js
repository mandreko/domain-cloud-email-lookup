var request = require('request');
var csv = require('csvtojson');
const { Resolver } = require('dns').promises;
const dns = new Resolver();
var fs = require('fs');

const TOP_500_URL = 'https://moz.com/top500/domains/csv';
const REPORT_FILENAME = 'report.html';

var data = [];

// Get the list of top 500 from mozilla's list
request.get(TOP_500_URL, async function(err, res, body) {
    if (err) {} //TODO: handle err
    if (res.statusCode !== 200) {} //TODO: handle err

    var domains = await csv().fromString(res.body);
   
    // Iterate through each domain and find it's DNS info and potential APIs
    for(let raw_domain of domains) {
        var domain = raw_domain.URL.replace(/\/$/, ""); // Remove trailing slash that mozilla adds
        
        // MX
        var mx_records;
        try{
            raw_mx = await dns.resolveMx(domain);
            mx_records = raw_mx.map(function(mx) {
                return mx.exchange;
            });
        } catch (err) {}
        
        // SPF
        var spf_record;
        try {
            raw_spf = await dns.resolveTxt(domain);
            spf_record = raw_spf.filter(function(spf) {
                return spf[0].startsWith('v=spf1');
            }).toString();
        } catch (err) {}

        // DKIM
        // TODO: Would likely have to setup an email inbox to check, and attempt sending email to it

        // DMARC
        var dmarc_record;
        try {
            raw_dmarc = await dns.resolveTxt(domain);
            dmarc_record = raw_dmarc.filter(function(dmarc) {
                return dmarc[0].startsWith('v=DMARC1');
            }).toString();
        } catch (err) {}

        await data.push({
            rank: raw_domain.Rank,
            domain: domain, 
            mx: mx_records, 
            spf: spf_record, 
            dmarc: dmarc_record,
            includes_sendgrid: spf_record.includes('sendgrid'),
            includes_mandrill: spf_record.includes('mandrillapp'),
            includes_mailgun: spf_record.includes('mailgun'),
            includes_postmarkapp: spf_record.includes('postmarkapp'),
        })
    };

    //console.log(JSON.stringify(data));
    var stream = fs.createWriteStream(REPORT_FILENAME);

    stream.once('open', function(fd) {
        var html = `
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <style>
            td.details-control::before {
                content: "\f055";
                font-weight: 900;
                font-family: "Font Awesome 5 Free";
                cursor: pointer;
            }
            tr.shown td.details-control::before {
                content: "\f056";
                font-weight: 900;
                font-family: "Font Awesome 5 Free";
                cursor: pointer;
            }
        </style>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script>
            function format ( d ) {
                return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                    '<tr>'+
                        '<td>SPF:</td>'+
                        '<td>'+d.spf+'</td>'+
                    '</tr>'+
                    '<tr>'+
                        '<td>DMARC:</td>'+
                        '<td>'+d.dmarc+'</td>'+
                    '</tr>'+
                '</table>';
            };

            $(document).ready(function() {
                // Add event listener for opening and closing details
                $('#data tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = table.row( tr );
            
                    if ( row.child.isShown() ) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child( format(row.data()) ).show();
                        tr.addClass('shown');
                    }
                } );

                var table = $('#data').DataTable( {
                    "columns": [
                        {
                            "className":      'details-control',
                            "orderable":      false,
                            "data":           null,
                            "defaultContent": ''
                        },
                        { "data": "rank" },
                        { "data": "domain" },
                        { "data": "spf" },
                        { "data": "dmarc" },
                        { "data": "includes_sendgrid" },
                        { "data": "includes_mandrill" },
                        { "data": "includes_mailgun" },
                        { "data": "includes_postmarkapp" }
                    ],
                    "order": [[1, 'asc']],
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "columnDefs": [ 
                        {
                            "targets": 9,
                            "data": "spf",
                            "render": 
                                function ( data, type, row ) {
                                    if (data != undefined && data.length>0) {
                                        return "<span style=\\\"color: Lime;\\\"><i class=\\\"fas fa-check\\\"></i></span>";
                                    }
                                    return "<span style=\\\"color: Tomato;\\\"><i class=\\\"fas fa-times\\\"></i></span>";
                                },
                        },
                        {
                            "targets": 10,
                            "data": "dmarc",
                            "render": 
                                function ( data, type, row ) {
                                    if (data != undefined && data.length>0) {
                                        return "<span style=\\\"color: Lime;\\\"><i class=\\\"fas fa-check\\\"></i></span>";
                                    }
                                    return "<span style=\\\"color: Tomato;\\\"><i class=\\\"fas fa-times\\\"></i></span>";
                                },
                        },
                        {
                            "targets": 11,
                            "data": "",
                            "render": 
                                function ( data, type, row ) {  
                                    var str = "";
                                    if (row.includes_sendgrid) {
                                       str += SENDGRID_IMAGE;
                                    }
                                    if (row.includes_mandrill) {
                                       str += MANDRILL_IMAGE;
                                    }
                                    if (row.includes_mailgun) {
                                        str += MAILGUN_IMAGE;
                                    }
                                    if (row.includes_postmarkapp) {
                                       str += POSTMARKAPP_IMAGE;
                                    }
                                    return str;
                                },
                        },
                        { "visible": false,  "targets": [ 3, 4,5, 6, 7, 8 ] }
                    ],
                    "data": DOMAIN_DATA
                } );
            });
        </script>
    </head>
    <body>
        <table id="data" class="display" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>Rank</th>
                    <th>Domain</th>
                    <th>SPF</th>
                    <th>DMARC</th>
                    <th>SendGrid</th>
                    <th>Mandrill</th>
                    <th>MailGun</th>
                    <th>PostMark</th>
                    <th>Has SPF</th>
                    <th>Has DMARC</th>
                    <th>Cloud Email Services</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </body>
    <script>
        const SENDGRID_IMAGE = "<img src=\\\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI0AAAAiCAYAAABiFFqeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABM8SURBVHhe7VoJWJVl2r45bLLJ7gKyuG8YsqiIu5ZrCdmopU6l1jSZZtRkajZqjWaL9ltZbo2NS6mZuVSOtmlamgohoKLiBiq4IsoiyDL3837ng8PhHAL/6f/turi9Xs+3vMuzvc/yftiUEahDHWoBg/G3DnWoMeqMpg61Rp3R1KHWqDOaOtQatU6E/3d5sw1sbIyXdfjDotZGk3U1G2ev5xnvqsJQzP+szOjh4oQWTbxoOHWW80dGrYxGun7wcwri8hqaeBz+lmlGYOAz71TA7pa6rQwaSv+WjlgcWx92tnVR8Y+MWmvPhkZSXFyM4tu3jY3XxfyVxusSPivhtXkrK7nN0bVyanW4S3FHW/5OwktN/Zl4MGnFJSWqlZaWqlYLh/j/Cp1eadVB51P6/R686vPWdi6dpuroqHV4+vDnZEy64YtSEmQOA+Xkm1oG20LjAxOIoQ1s68Lw5G4xPMncRfRSCYdSsGffAeZO1+m9SuHqyjwowA89unZC06BAGAyGuzYnyrp0GV9u/w4lIhvy07t7V7Rq0awSvcJnCRWSeiIN++MTcSrzPHJvFMGWfTxcndG6RTCiIsPg79f4jnm9cfMmVq79AqfPZaJXlzAM7t8XdnZ2xrfWUVR0Gxu2fIWbuXmMKEDzpsHo06OrosMUd2Q0E3N8UEbGzXGnRiPznjl3Dv9YsAQHElNwq1AmkJ0o49joEOu7u2FQryg8M34MGvj43HWGIzz8SoMf+8JMFBQUwGBjwNxpkxA7uH+50MWYDqUcwdLVn2NfQhL73SKXmhx1LRhsbeHpXh+9oyLwlzHDERwUUEVp1UE8xPwP/omPaDSlpSVwdnLGwtkvome3qN+U2c2buRg2/jmkn7sg0sfQAX3xxoznYUuaTFFzan4niLAzzp/H5JfnYvcvB5FPgWt2zPLcoDFZWlaK69dzsHbrDkyYOgdn09ONfe4u6EpRrt2mTBm8jtvMA9d8vgV/efFVfP/TPuTl59PjSPjQ3ut9S9jv6rVsbNz2HcbG/R3f7dqjea5aIDk1TRlMaWkZ1ynAsbSTxjc1AOkQQxa6yIJFWDQaPaZVaZzJyYsls3JeFqAWJOQ/S81CmBflL1n1GVLTznANEbQN/Bo1wMihAzF2RCyiIjqiXr16qq+sK5uutNTK+ncpROmffLYF8xevQg5Dh/ApsGXIaODrgzYtmjMsNYOXp4d6JhB5X7h4EdPfWIQfdv+s7msCkV/XiBBuODvloTzotcI7tDe+rQE00pQuraFKeLpVVIT4AgMKmU9UAQn6KSsfsxOvokSZovG5EWKZzmX2tMSqtijMPBDkgo+7VpTcsnQ2PciQMRNw5do19axpgD+WL5iNJn5+aowIfDd35uyFy9C7azjinhqL+m5u6t3dBOElMekwHn/+78jLy6PybTFv2rOIGdQfB5i7PD19LnOFXNVPaA/v0BaPjbgfXSMj4erirHSVk3MTu/fux5I1G3HqrOZNDfS2Pt7eWPPeXIaqwBrxLbnhlq93MG86hQF9uyOiY2iNQpwens4yVSCViGUuNO+VGoQnSdIuFAOnCkqqtvxiZDJps7nF8rmILrOwcivjszxXLu5hueU7GxcxwdXsbFy/cVNdi0C6hHWEf2M/xaTcSwLXu2c3fDT/VUx/boJVgxEBS9O9on4vzRym70yb+VhrqFl/7b64uAQfrFyP3LwKg+nXLRJL3pqNQf36Kk9gb28PBzYfb088eP8ALH1zJkLbtlQGY2trh5DWzcuVrq9luqb5M3vKbNgDgzD9+WcsGozer1r6rbMPiym1jJfJLEFzrf+9XS4MiudhKFeEH0xOQXpGBoICA5SA9dYs2Pouk9Ly+PE0/Miq61DqSeQzjosywtq2YvbfBQFN/MsFJ/nC2k3b6BUpLN73iAqHi1M9lUMcTTtLzsrQOjAIA+6NRki7dlRahcCFPknSxXPs2s/K52yGoqk5PUD/np3Yw1Q5nIn9Dx0+goTko0qm0rdjSBvMm/E3uNevavz6fSCrxddfjsOMee9j9LBBGNCvlzIq0cknG7+kR+AmK7NhNRmAe3tFI/nwUa6TirzCYtzTuqnycr8mHys3umFD7qVBaifxYsTHjh/H9z8fULlPISsmvwY+6NG5IyIj7mFYq8ZajKgSnvJu3cJXuXa4nm+hBCISzuXho1SWw9bsxov5h4WSWgiObeyC9aGVT4Rvcb0Hx01G2umzSsjSr2lgEwwf3Be9unVB0+Ag9tfco7mQdSUu+mgV1m39RuULZcZ8QSCJdCNfXzz5cAwefmio2tHHTpxBzNiJ2lp8f3+/Hjh46DAusFzWx8rz+q5umDxuJEb9KVa5Z+l/8dIlzF24VAlcQoBpf1dnF0RFheHH3b+Qp0IqzoA5L01m0p6hwo0o3I7rz5/xHEvgfpV4MVOBgjyTZNnF2bm8r6w59LFJOJNxThSHgX26oVmgP/65bjMKKEd7eweGxElYueFLJB05zhGky80Fq9+dw9K/OQ2kCMtWrsOqjV9VkpXQLzx2Cw/FkZNncPnKVTU2ltVTjcKTgjWD+B3g6OiIccOHwonJrghHhHU6/RzeXroaoyZMxfi4l7Fy3UZkZl2s4v2kZJ311nssLzfjes4NoxAkWdbYkvvMi5fw5uKV+HjNeuZH2ng5LpDqQk60t+7YyYTzEu3cVpW7glL2y7lxAwtXrEfykVS1rtzHzXwD23fuVUYhfXS9yzqSr3z73W71TocktWkZF3ilbQYfT3dEdxGPVBkZ584jITG5UpPy/UTaKf5WPMvNlW9+DCWkXdKIA0ks32mQUiEJDX4NfWkkrjh28qzxwLBYGZdADO5/lnyMJas3KFkJ/TpkrJzm7/olXhmMMuJqbOC3T3x+Z4gwY4b0R15BIRavXo9rTIyFaEmAZTfsiz+kzjSW0zBGxwzAmJEPclc7K0V+vuVrbP1mF25TIDJPy2bB6N89Er4+3kzmLmD7rl+UQYg3+oDC7RLZURlnJXBc98hQ7qp+3K0FWPflN0g+ekLRcONmDrbs2IWQtq2x4pMNSEhKVWWyrOXm6oqu4e0R7N8Yl65dp8B/VUl9BUTqJVSQ5DJqGTRq4MtxLmq8DhVyWIqv3Pi1mFYlXWnq1may5UZ4d87LSsECoe/SZfEIYCh2p0dyUp7nyLE0bkR7GpJ6VY798Un4dPN2zUNyrL2DPcLbt0VIq2D17OeEw2qzlm9MfXELsGw0MkCaKQeEMC+Q/Wj2SgMflrCPpfVEThbHEA5k4NGHY9ElIhSbtu2g+49HRiY9C3eKMCieIYvKf3fFpzidkYlXpz5L5sqwetN2Ggx3EyePDAvB28wVGrNclzGC2EH34skpr1G4V5BPKa7d9G88Sq+mQ8Z1aNcKb82cAk8PZupEaId2eHzyKypBZ9qDlNQTyM7Owbad+9R5kYzx9HDH26/EIapThMrJRNAJSSl4cfZ8nCed2vqkW3FcIQ1TYzGF9JDvebp8LYKhzdJRR0z/Hnhm/KNwYY7k6lgPJcxZ7GwdMH/JCuVZ5ZhE6NnAakpSAbkWeU94dATGjx6BevUc1bOLly9j+px3sOdAopF+66gantj/Zl4p3W0Jd1rlJs+4edDQ2RZ+TmzyqzfeN3G2R5v6jghxq9rauzggwFEqIuM6ZpCQ0qZVM7z07FPY9PF7WLXwNYxnLtKMyZ4kc8KIHHNv/mYnvtj2PVJPnlaxXZ7LWCcKbDHj9ay33sfstxeptnbLDrg6Oan5RXiJR4+TD61SE4gSozuFKoOROaQF+PuhRdMAYw8gl67/DBPzLBqeEibHPD5iKLpHdVbJqcwhMT8itAOeHPVg5eN6G1t4eHkonmXoRXqGXHMXQIhIJJSZNjt7O+M4TemyVn03KT81hcq6gfRyUyf9FUFM9H3pbZyZ0Lu6OtObOpBf1U1Bjkckx9FkZYMobs7xY0bAif1lHuG7IXO/ac8+qcp/ZdxW9CSo4mmK6SqWbctF1rU8nTw2uZKJyhDV0hFTuzQ0vjNFGZzpFrsZcuHjzrrbbFFxq/YUorhZaxBipQnzkWGhqo17JBvLVn2Gf23YrEKDeB857HJ1rggz6ixnXzzHGh+YQBe6zHv9Ri5DEK3eyJLsXE8XF/VehwjQ0cHBeCfdynDl6lVVZcgcYsBd6NWUYE0g9xGhISr8idcQlJHWZv6N8B3XKaOXunztGnOTw+jTs+JIX9aLHXwfIjnWVKY2NMSFy9eosxaBq4sTfGiAGvGyHlTB4EYPI3NUgplyCljUSB6jgRVc2xaox1zSFPqGCfBrjKMnTlaZwxQWNUjd0Hj0Jl+cjb+URSEns7W1oQs0bwbV6EzgQZfnSeGZNi/GXDenCmWYQpSWxx145QpDAq9FoHrz8fHExCdGozGTPLnna5zPuoyiwtu6/NRzB+5MB1YP5s3RwZECqqeEpKqgyoWAVXDKCigBasYnv/qJrjkkHZA+5UbIfn26dlJrC42SbC5fu1FL2vU+ROuWLdCvdw/ca2x9e3aDJ40h/XyWei+0tG/VHN6enqrS0eHi5Kx4qhFMGDJZuhIU7bq1mPJvBuvb/g5QzTpWIYReyMzC1H/Mx9Q581WVUp6MEfJeEtRbhUXGJ1oO1MjXS70TZYhn+NvTj+P9OVOx6PVpFW3uNLzOEnfhq1P4bpoqR73FC+pCqyHBvr4+qEfjE0jOsHvfQbW2KYTmvfEJyGfeoEH75NGhXVuEtm+tdCZj4pOPsGxfrM6SZIzOgzSB3Eul+Pq7y9gnX90b6N2G9O2uQtadwJkhR/IwDULDMfVR1ZQHoSXt1Fmkn9MMtVxGFmDZaKx9qfovQwhNTjmKcc+/gh279mI3S77nZ72BI0w+5dyhkIYih3HvLP0Y2TlaVSWybdM8CKEhbVQsFxTdLsJP8Ylo37olurOk7cF8o1vnSDShq126agPWbfk3vLzd0TkijAZn4patsakUrF3a2hgQFNAEfo0bGBVbhk9YhWz//kdVlQkPUr39cjCBpf8Wemm6ZQVtAjs7W0x8/GG4ubiq8fJBcsv2XRgfN0N9jJQPscJrAUv1a0y4t/+wC0+8MBNJlIEyGHqWcBrdfX16qvlMYap0cxhtEAZeGEhLWLs26pl4yf0s599fvlIZroR2OfC7kJmJuWKoBZqhVgeLpvt/ZDMksBALlq7EmfTz6txBsHtfAuKTjqJFcJA6qU2/kIULxjMaEbp86o8d2JdJnBNGxfTHmx+uVErb9dMBjKUi+kZ3gkd9FyauV1nx7EXWpSvqK28Sy+hlb8+CYw13qwhYZCd/ySofEmPv64kFyzOU0nNu3MQUVhqRoTvQqmkQK6bL2EODV+cl5QLXtCY0R4bdg0lPPIIFS1arHS65mXicRCanDX284csQLLh05RrbVaVI3WD8GzfGK3F/VVWOVIo1hanehZeRDwzEzr0H1HmSnE/JEcbexCPoGNKa3rEQe/f/ikzy8VsGI7DoaURQd4La2poLk9nXXpqI8ND2KjaLgIVo2QFJR1Lp7hNxnqFLNxipTMRQwlmpSOL2p5ghGNg7WiWngmMnz+BDJs2vL/oXVny2VRmMQP62pRdzC/mSrCtTwRqflRjRwseo4TGIjuio5UWkUQ4Wf9qfgBXrN2HHzj3KYEQ50tccMmb0QzF44ak/M6F1UcYgc8ifyMqXbPkEIE0OIrXSW6sIJSmdN20ic57mFuetDcI6tlMfSPWKT2Sawmpyzedb8cVX3yiDqUq/ZY1ayWkYY8monJBWaXS38j3GUjNN0moCIbCJvz+WvjULk8c9osKJHLWbziPXBs7dqIEPpjz9GOKeHl9e1soh39zpcao092LZLH01pjVFi3Lkm8tTY4Zh1ovPqM8IrOE04Rj7WtxZfG5DQ1N9jCKStebPfhEPDeqrDuiEJhkqZznSvyHznrGjHoITE34lC/4znVvOc/48PBYr3nkV/Vg9ubu5VZGZ4pXG4uvtjYfuvw9rFs1jSA1Xz8rBKRVtbNaUKqFChSXOzxtFp3yKmTB2jDJc+SM2A4sXkYOinxAPPvrBIUoHYuSyZEVmWRlVvj1JafnpzlT+0kVW8KMRy5/6rgb4eEjGrg+r6GRLpsOCmqi/PKsNhARp8ueSB+J/RcqJs8jk9W3GWk93N7Rp2gS9oqMRGKAxZA6JyScYgvaw7D56Ol0lo24sUVuRll7RndGsaXC5oWVnZ+OzzV+VK1T+tPKekPYUoMaHzLXt2++RmXlRsSZf1UcOG6reyxj5Y6pDySnK1Z85d1F9fmgRHIBBfaLhxr5fbv9WhReB9ueelb2EzCEnsMeOn8BBhqi0M+eZ17Ca4jspqVsF+6MraQoKDFT5kOlYmXf9F1tViBEEBfgz1+lV2aiIpMNHGW4Oqmsx1pjBA+DlxcqLc4mHOZWegR9+3Es5n1ZnXwF+DdCrcxjCQu/B5q+3l8/fnClCH1Zy5vNXMRqBHIRVB9lFlqHt8DuFTor86mWtTCdz6s0aZIzWtOvqxongdFh6r8+lw1xo+nttHs2j6XOYjrM0tw59Dq1pz6Srvpa1cb9Fu0CfV4d1+rU+Ov3SajK/RaOpQx2qQ2UTrEMdaoA6o6lDLQH8BzOxFZ9Pw1SBAAAAAElFTkSuQmCC\\\" />";
        const MANDRILL_IMAGE = "<img src=\\\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAAArCAIAAABw2hRhAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAvJSURBVHhe7Zr3UxVZFsf7D5lBzEqQLDknRUVHGXdWMUzY2d2qEXUslWTa1dGtndVSAS11yOKAAVaygrBmt7RUUMEc+Ef2030el0vztF7/5tvqW7xTp8/5nnPvPd8bup0xOts73b//4z8jN3dRTk6uK/1IOnIZ/LKzc1zpR9KRy+CXlZXtSj+SjlxGZmYWf2iiuLpf6LZHry6RJsEZGZmu9CPpyGVkpGemp2egmdLV/UH31WVJg19aWror/Ug6chlpqWmpqWlpqemWdHU/0H12mdLgl5KS6ko/ko5cRkpyanJyiiv9SDpyGfySkpJd6UfSkctISkxOTExypR9JRy6DX0JCoiv9SDpyGQnxifHxCa70I+nIZcTHJcTFxaOZ0jed1zM+sHhDS05K8YpJTOAGSAEDEgxGr3lQ5GWPF3ozW7KVzVu/rETySL/AyK8wHETEip0p6bFqDJ7PhvExy9IWjI7HqMYjeGRqSho3GSHAbHgUXGbvqekoek7wZu9WHlxTY5UutQImY/OKsem+uixpxMbGx8bGxVnSR/251UZHR5uamhj9VEx+/vJbt26BGRkZOXPmN2brNQ8V6evrGxkZFeS5pnMMyIYRPTs7Z2Bg0Or2+YMHD9av36AwO3cWP3369Pno877+a0lJKXrs/fv3JYTGaIeGhu/evXvpUuvu3XsyM7Om9kWhL15q9QRY7dmzZyRpb2/fvWsPHNjwzOvo0aOCvHv3HnSKHckbLFHYR0efV1fXgLTFis4Y8vKWMiqQzKK0tByLDeNN98kl0ohdGLdwYSyaKX3TP4y3ly9f/vTTJlaKjmEl9vf1v3//HgCyob7Bmp49D2ttzZq17969k1S0169f5+YuYlgKo/AQTBU8uA8fent6rXKb3rLSsjdv3tLRzVu32M16LCXzBIw3YLQ3b9709fXn5i5mDDqexdrR2emBTm5v376FS1t+FmhVVZUAhoaGmLjYkayV3t5e7HTX2NhoVmByDUXnb0ne0sePH4+NjdHFnj17xahjpuq+uixpLIyJjYlZ6EgyGmlM4N69/+YtXmLmsrzQU15eDlUCYHr19fUUYmqexITE336rBmDV5wNgyC4rKyeDDYnMysq+c+eu5KS9evmquLiYyeAtLSmjNGS4efMWx50e9eTJE+x0QQU7Ojqu9V9jH9MLGTBeuXI1N2eRjoeGzo5O6eLRo0etra39/ddevXolFjbZhvUbdDwru7KySryPHw9BqvJy2PZeuYKdjhoaGr1WAAkBVG+C4N17sHhFTopy4jJiohdGR8c4ktaMPI0KNjaYKxQ7g1uev4K1zKzEi1JfV08hpubJyc69c+cO4eyngYEBkOgXLlzgHrUhkZyogCUnDTBHev6y5fRYUlI6TvBNW6wiuKGhgRJT9GXL8puazoEnCfLnrT9TAoUH0zFOcHNzM7cvnBVtKpLpsDL2/32/np/jtLKiUvCQlJyUrLzs9Z5eRXCD1wog6X3xojxFMBeBPp6PSUcuIyoqmr/oqBhRfNGtGU001vj33//AyChHa2sbNfU4rOnV1dVRCFseOuZ8hlow3HOc8yQhcHh4mCoLRsdnZGQqgoHRzMy1Zubi4hJFMI96rBAMMbW1tcqek5PD7pRULS0tlF7h0dno0gXrgAOcSbF2uYkwkufAgV9UHv44tyo0giFV7PxxlvT09mI3Ca5vkIHxp2JFpw7cSo8eeQjetWsXFhvGq2579OoSaURFRkdGRjmS1ozMJluB9vDhwxUrvtq2bZtwJgSgCMEUwpaBnXfixAkrdAyFvXL9+nV53Fy0hUna8OnpGUIwmXl5oRcU5MaN35VqO5jjXY+aILimVtnBnD3bJH1xr5vraRwPDUIw7eLFi5xG33zzR7VHzXX83Q96fmZRUVEhXgiGVOXlLFEE19c3TK2ASAjgmpAFZxJcvstkyBtyUpQTlxEZERUREelIWjMyB3T06DFhlGl0d/dwS4nryfCTzs4usbPPKIQtQ3p6Om9AAsjLW8Je2b//gBU6BtOUYypeEbxjx05KhsIjC+vXf/6qCOZW1qMUwTU1tcoOprLKc3ESzq5VePrtaPcQzLxGno1wZ8siJkl1dTWLQ8/P/q44PkEwpCovM9IIrp9aAZEQwFWlCC4v34XFK3JSlBOXEREeGR4e4UhaMzJL8Jc///Xs2bPMgUdKiYLkyP1247cccRix1NbWUQhbhq1bt8rK4HWM+bPD2trazKRjY5yHq1YV2PB8kiqCt2zZumL5Cgoq5LELpd8bN27yWqFHaQTXKDsEt7X9W/riHQpSFR4a2scJJlAaOvkPHz7CSzIl0/Nz0hzXCIZU5WUpdPeME1xXP7UCIiGAD4QJgsvKsXhFTopy4jLCw8LDwsLDwyIs6ZNuzcjkZuOGjZR+eGiYaYgR5fTp03GxcbxZyCP3H4XQ83B01NXVS+2mNkL4tKWUer98FN257SG4qGhzdFT0vn1/gzmVBOXGjRsxMTGCl1j1klVTbRJMTvbHyq9WyuGBq7KykiNR4aGB818SskwvX74snwMgOSesWUyqA7HHjx0XPARDqtiRLKPunh6JZQUzsEj6t3YVI1F50LOzsnWCtR1s0qP6ErzotsePu0xphC0IX7AgzJG0ZmQSvH7dBgaxefMW+ZaglJyTkMEQOUXFwu6kEHoGCsG2s3KY8wdDE0WMLS3nY6Jj9B7ZPbcnCC7CYpHRwaOEoJgET45SBDc1NfGhtWxp/vbtOwauDUjUixcv1q5Zq+OhsP2ySTAA7mB2JCtAwKTiJYOS6XjIOKYRDKnKy1miCG691Lp69R8KVhVwMiG5kthbgoSArEwPwazXI4ePFBR8DYa2amUBY1Z96dKr8WMuY0FoWGjoAkfSmpFJ8LrC9Vh4W+OTkTqOjppfigyaCbBswWDk/qMQeoYf//Sj7Azk1wWrs7I8/yPgju07pJrcfNxMeo+8n9++fRsXgE2birDQy9q1heojFTsEMxI9SgimMQxuBLYIRZRlhH7y5EkbnoV4eZzgCxcu4s3PX04SsUA5S1PHs8+OHT1mdm8RDKnKyyeKEEyjR7qTxjB4O1F5ICAzI0sIlnEqGMU5deo0S0rv0RM1+fHTLiM0ZEFISKgjKeNmBIWF68TCrh38z/VTp05RFCwMa4Lg6hoKoWLp8urVqzIZ7kI1AabKDnj48BEuZnjwl4N6j0lJyYpgvqnETuyRI0cAix2COfz1KCHYHOt44xGOeRnct3cfZwD96ngWIseywPgiJxvnU1lpmawJis5LEONUeLy8ZlqJx/jUgVSVjToogvVG5p6eXq4wQTL3jPRMIdjWGOeZM2dsI/RETX78tMsICQ4NDgoxZbAlfdB/t1pjY+PSpcvEzkA5rmFIMAyrrKwcDGdjaWmZOcrxWF41ObK6uro6Ojr56mAQKj+wQ4f+0ckK7+qqqjpBTrEjef3hc6vL9HQVFhYqOx82bLXBwcGBgUH6gnJ9nOfPnzcDurq7u7qRvCE3N7ccPHgoNyeX44vJqzyCx8iOBEnM4X8dFgzXDe8TVp4utpRpHMcz+JLiEsnf/HszSDUXzrCKyirp1wq1xmCNv6qqiqWj+uXrmUJNYDyS+nTs3btPr4/q1/b4KVdwiMFzUFCwI2m1UCnQxzBmB94wrAWmx20nx6MtioMXO14plrJLobGzS/QoMByM5n83iU9kEdj6Ykfq/6ZDhqk96pJsHDaCZ3dKNmRERISMSk4IHc+SEjyBeu8SpXrn8BedPNhtSNs4FZIxqL506dX4MZcRND94/rwgU863pKt/9roD1/wgg+d58+a70o+kI5cxd+78uXPmzbOkq/uH7ptLpMFvzpy5rvQj6chlzJk9d/asOa70I+nIZfCbNXO2KWdZ0tU/e92Ba9ZsY+bMWTNnzMKKdHX/0H1ziTT4zZg+05QzLOnqn73uwDVjpsHz9MAZrvQj6chl8AucNt2UgZZ09c9ed+AKnG4ETgucFhCI1ZKu7ge6zy5TGvwCvpxmygBLuvpnrztwBUwzeP7yiwBX+pF04Poi4H8CFog6McHEgwAAAABJRU5ErkJggg==\\\">";
        const MAILGUN_IMAGE = "<img src=\\\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALgAAAAxCAIAAADm7vSAAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA1TSURBVHhe7Vz5f1NVFp9/YqqjzuKMxQVRFMcREUdHUcBxQ8FlrCJiNwoUQZYWlM2F2mJZuoBSyjKWVhihQEtbtqI0XZIu6ZZ0oUvSlG5pSds0XZLMae7tyclL0qZp+NDwed/P9wfyPee99N33ffeee+8LvzOLEOECRKOIcAmiUcaNmtraT/2DkFy93SEaZdwQjSLCJYhGEeESRKOIEMJkMqlU6u6eHv7ZAtEobsJkNBoaVbp8mV5RPdDSahoc4gEvB1hk7bowsIJ/QPCRI//lqmiUcWGop7c9Nb0qILTo6Zckdz2Q8/v7rPTxLZ23sGnPfkODimd7J6J2RlNDyOWlTBeN4hJ6KxSKj4KF5nBChV+gQa3hR3obQpavpIY4lXqa6aJRxkDftXrl0hUCK4zJ3HumqqPjTEPeNx5FRn1PDVFSIme6aJTRoM24mPvnaTYm8PGVz3mz4auo64k/dZzN0KafV++Mgc6m4IEnbdIsLHv13YH2Dn4uL4FKpVq/IRysADXK4SNHuSoaZRQ0fL2T3nVwDHjC6Y03mboLS+q37Mj948P0qIKpM3tKyniOl8Ay61GJsx7A2EbR7D9E73eVf6iLfcNAa1vNqjB6bL7vDO8tWRCiURygp7hU8of78U43fvs9DxAY+/r0VbXdshJwBpcI2n4+RSvfollzYcbEY94J0ShCGPV62fRn8R43xfzIAxZAfQoz5IqFi6FYwRzoM5RLQjrPX+ZJFnRlX6VeqQ5azQPeCdEoQjRsjcC7WxW4iqsWQAErffQZjNqz0i9gUNvJs83m1p9P0ij0QDwwKrTazvT0c6mpZ4CywiKj0cgDFgwODpZXVGRmZkH04qXLvb16HnCCoaEhpbIq+8qvqafPnD59Fo6FEoTHxgMXjTKk64YHRhN/sDFil/r7WJ20iAduMkz9A61JJ+BLgaqI3fDvgZZWHpsAnBrFoGrCbqBo1sumgUEeMJlq12zCWz4KpdNn6xXV/Ciz+dr6LRhS+AVylYC2fm3ttatXc4KCl1MxLGxTbe2ww/r6+pKOpQjWOYKXrUhJOQ7uYWejaGtrTzx0RJAPDAv/gi2jURG+mh1VUCBFce26DUwEjGkU8ETl+0vpkM1YOn9Rv+a6rqCQiuyQptgDqMjnvs1EATABiLaTPjKLip0Xr9g/wHn3Ptp2PJXluw2nRqF1KB1KYDqD+jB9fAuffEG5dAWULzBVLnzieRqVPjwTK194wmBgwpBeWcN0BG39mJg4+hG5MvSzykoFm7U65L79NuMjQKFUCgwn4LmMTPpxgkZp/vEwXqM9oQVgBKcKO8ojRhl9lav5h0PsEPfgxCgmU/6UJ9gXlM5byEWoNi7/hl8MhOemr76Rx0agzbwkm/Ec5lQs+pgHbCdQ6ug4ro6Atv5EWF9fz89oNqvV6tFdYs+JGKX12Am8QBfJDvSIUUan5K4HBzu72FFuwLFRaPfYnGBdayp+9hXUFR8GOdv/G+ruKZ49HzNv5ORzXdctuZN3yPbNQVsfGBC4LDHx8MlTqbFx+/wDggVR4JatX0E0KSk5fOOXVIdRhp0QipJNmzbT0DffRkhy86CPAWZlXfhs9VoaZXTbKDBYC9YkYciGSd+N33KBMPLm3v0QjTKyYz1lFKgWoDTp+lUC3wjVScm/XqPR64lJ7Cg34NgoMI7g2bEU0uUWoCif86bJtrQUAHoa9ETlB/5cNZvLF3zIT+LjKzA4bX1wBm7CAdLSztEocPfuGKxt+/v7wTQYglKG6ZcvZ6MITDh4SFC9GgyG3XtiaA7QbaNcW7eZX5qFdRu3CzYu+q7VQ91Gc4As5CmjtPx0nOkMxt5eOtxXBX7GA+OHY6PAGdmp4e/gEkyCtkfiV3acyeAq9LfJ/wPn5v3lkYp3lvSUVXIVTuIfypKh08O1E6j/8SQ98nImMtDW37M3lqsWQIkKtSpNaG2zWbO5cPESjbKSdv2GjajAvx3WuSBu2/41pgHdM4pRr6cr0fTZoIDKTLBgzXSPGKVo1lwmUlD7lrzwOlfHD8dGKV/gx0/94htcIp0BdKEwB2MiDCtMZISS22gwsFB7ajrq6Ak6imuzLjGRgbb++fMXuTqC6F17MLpufThXR1BdXYNRoE6na2lppcrl7Cs81Q5lZeU00z2jQGWG1wXU1/CT2KN2zUaayUSPGKVu01dMpIDKARNgqsHV8cOxUWBwZaeGcpVLRCx+Zh6XzOa68G1MROryZSwEc2MUOy9kM5GWwy1HU5jIQFu/eGSrFpGQkIjRyEjhArFKpcYoUKvtlBUW4UcYyPR6p6ssUMoEBoVgsntGafzGuh02+v3oSMvETCATPWIUTfxBJlJAkYQJ8BhzdfxwZpS57NQV737CJWqU2fO5ZLs6wnhDUsBC0M2iCA8cE7uyr6LYfND62hiAtn5VlXUBhuFYcgpGY2LjuTqC9vZ2jALBKNCF4MeVoWOsBX+5eSsmu2cU2k9ULFzMRIegzw+QiR4xSmvKL0ykaD95FhM8b5ThhXnLqUuef5VLMPS8/RETc++Zah16JNYKFwh/t7Gvj4XaT59DHXsU2gPBNTCRgbY+3i1EcvLPGI3f9wNXR9Ch1WIUOF6jbN68DZPdM0pVAC/IgJV+AUx0CH1VLWYCmegRowjak+HmGqVm5Xp26vz7/84l2961Iy2Lq1jM3vsoFLO95Q6KWSB0UdCXQBdCN336aut4qgW09SduFKmskCoDA9zZDhGyPBQz3e1RrKvV8pcWMNEhBGtRTNTEJaDi8HCYQGECsFtazHQbo5xKYyLFzTVKU8wPePbeSiUTaZ9ZOn8RE50BpoI5d0zBfHvaNwdt/Ykbpbm5mSp0si1AQ2MjzXTPKOqdMXhpkjvvH2VpS/ByDxPhEULF4e3sb2rGBGBPaQXTb7FRaPeoitrLVbNZ4Rdo1SOtugBQ80unPY2ZDujji6twCNr6EzcKiKGr1qAS8V2Usy3AuPj9mAZ0zyi6PCm9QNpoFIPazoIH/0Ezmd52PBUV8BlMtpmO6LqSgwlAeA6ZfouNAsBleKjhceEIDESXAapD1hpUahZCaNPPSx+eiTmVH/hD54Efh+nj69BktPU9YpSkpGQqHj36E0umOHM2jeYA3TOKaXCo4KGn8Bpz/zQNRwcE3P7SV97BHEYWgm6bivb7MnQfB4ZvXO289UahFYlmv/XvBh+gPsw7pigWBzdG7Krf/C2UwHQdEFg0c85QT69pYLD12InqZZ+Xvf5+ddBq7DYFoK3vEaO0tLTSeS/wu8iduBOkVqth9kSjjO4ZBaDeFU+vHVoGpkL9muvDMZOp/Zczgh1TRsuhw7+Nohtk8DQ2Jxw1NKiMBkOPvFywXU9X8269UYZu6PL+Op19Qf6UJ+jrj/AHSe56EL/eGYuf+7ehcRw/7aGt7xGjAHJyJFR3hW4bxdjfL9hbcYX8YLD1kWRByDF9fLsLS/gxk8EoAFqgwfABDcEDZjP0CiUvvoFRAaFvvLZ+i/UVFtdAW99TRgFkZGTRkD3Dwr+gH902CgD6D9lj/xS0hg19fCvfW0oVfqQF5W+NbIQ5Z+OOaJ5twaQwCvR7ssetl61cEmJz700mbdaluvBt8pffguE59+6HZNOflc99e/gF/bZ2nmOBNvNS+QI/+sKbQ9DW96BRAJLcvOUrVtEE5I4dkTpdN1UmYhQATE8qFn2MjUaZf9/jrSm/0DsH5IdZYOofqF6+lkYpYTxqsn0bFTApjAIY3sQiW+dlr7031G3z24UxAS5h73rBX0m3DO3BXnlkFNxpQFlZOUalUr5LgICbDfUHEj7ywAg6O7vOZWRG7Yxeuy5sWcjKbdu/PpCQKJHksqkQe/GAsW9kwbCpSYPfmJV1gYkAMEqBVIbkqi1uXM2reGcJazR4iqCrgPp0sOsGhOidgwkOy6foLpJDJcfGfXj8imbNVXwYBB2Jw3fXm2IPsLcegfR9QkRvZRUmaOISuDp+jGEUwPACEVkRgfpUsJnnDFCLVQevoceOvmTp7dDsS8RbYj8ZRFw/fAwbBDoJrk56jG0UQOfFK9Bt4uUBy974D5TxQ3YP7jBMJpj0VwWE0kVYdghuLN+WgOIdL5a+hiEAnU7CLIGrkx4uGQUAj0jxM/PwChlhTCmdv0j56cq6sK0N2yNrQjfAzI3+wgMJfQmthW9L0CpVFbGbq3ZQLA7GNPmcN7k66eGqUQBwp1VRe3PvmYrX6QrzfWdM5A08L4I6Og6vGiYB9qurAMHORu3qjTww6TEOozAY1BrlJ8sFw4pDSqc93bRnv7HXu38X6Dr66hqoCZQfLxN0ojBSQweMCUAY03ls0mPcRmHA/0gHOk/ZjOfy/vYYu/KCB56sWbEOKvyu7Ku3zX+95Dqql32OJgAWPjWn/stv2o6ndqRlQamLL/QwjrmxOqngplFEOAQ8P2AO6gZnlE6f3d/cwg/zBohG8TAG2jsE44s9YX40yku1kxOiUTwPk9HYduK0cM/cQhiph39R4dZvnm8tRKPcRAy0tmnTz2viDzbFHmg/eVZf7dJP8ycnRKOIcAmiUUS4BNEoIlyA2fx/Rr2dHbZ6S/AAAAAASUVORK5CYII=\\\">";
        const POSTMARKAPP_IMAGE = "<img src=\\\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALIAAAArCAYAAADc49LjAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAfASURBVHhe7ZpLi1xFGIb9Ey7cuUhAozOTTLwQFZMh91FMdiKCISpEMREUBGNAEARREPG6ExEkXoirgIiCGEgWblQICIJZ6MqVG3du2n46vj1vf36nu/qWdE5q8dA9p766P1Wnzum5oXNpvVOpXOtUkSutoIpcaQVV5EorqCJXWkEVudIKqsiVVlBFrrSCKnKlFUwl8j+/Huzx10/7On+c39vnt+/39OD7nz/s60FMVsan795TzPkzu9IyKosDc5TNXRY7S8YW2cX9+au1Hr98s7svrkBePrlOuseSRjmUt311c+fGm24uZnXb5s4bp+78X7sqi8GJx7em85bFzpJikREPASUjkiK0hCxBZUhsJEfMrOOjIB9tyOpZBNiZfl/g9s2LhRdZAv598UCaPi6Smt317Vfu6rx28o6Bjn/23uVbkj4hDtLh9S1p2aOgXsjSZgES077rUWQ2F/p/4cvLYyCy2Fky1o6cXZ8WCf3xWzuKOo74Hnf+zM40LsPvCPPqjySG61Fkx+cpS58lUz3sOYgh2LVhHFnOfb5zoONND4eseHZixZWcl2kHAnOcmfdxxI9KVeSN+czSZ8nEIkuO+DDXBDHENx1NfCcDzwO+KPyIcSg5XhCrB1La993pnb3vszoWDeNaEJmxmPeCBo0DZOmzZCyRJa/E5ZMBQRrtwC4c37lOuqSX2OTz2Cgy15RPeVTni8dX+3EHdt/aK4uyfVG98PS2zv5umuIQDFgEJZNIDLGHDm4ZyEs7ucZd4fWXNu4G3Bn8TgGK87O8/galU6bqVDnUd7xbX3x1FWOg5K5EHfEZY1R+6vJ2Co2fxqjf326a5/e6/LrGNZYdx2McikVGKonE9yymFOSWoMiH0JnIjhYF+Z58dLkfd+Thpd6AAmkXv17rPLh/UKjItu7k8XAZ6xBMbJYvgmjKc3j9tjQGkEVxWTqyDqtTog2LQQ7VETnxRP4mwaGNElTEORHE0eZ4vVRk6vK0SFy8JRSLjESQpU0Kckro0+8MDkwWL3wgvNMMsJchmgYum/w4eeQljp3Cr4OLjCyxHv4G8ipO1zwu0lSO/521J9vJovz0hWu8DeJ7TPO8lKe6vf5MYhhnR6b9XiZwDbJ+jGJmD3vTgNBnP7xvoFNZHKLGCfRzaEzTTgbU8ePZtc7etY3jBsS3Hj65cUCp3wffRRaePuyMHAUjn7c31qUYBOzHXBiMyQSQNE3p3g7KiumCvB6nT6TmVVuWR/EQ02J5WdvGYSqRkQM4ajDwoLOqYMeFUccR7xhwxn35ue29I4BPhuMTH3cJyqNt3EVol45Fl87t6Za1cQyIk6dJAvJ5GnBN6bMSOe6EwhcV/c9ifFx8PErx/kBTm+P8NLXH8Xi/HhfPtBLD2CJLXCRFDgnC3wwKSF6+S2iPJY1yvNw4UKN4qPtgoIUETx1Z6acdfWS5Xyf10Q4/FrGTeVk+kC4GImWDrB9psrRJRPbjUVNMk6QlMaPwfxMoFTnre8Tjdc3Ht2QxlFIsMrIgYJMcJagMScanyogDFVlZ2dxZXt7U2bPrlt6EaWGI25c29WPff/Xu3mIb1j4fUJ+U7PyHnDxRI3aTdGJRRSZNfVZ/FD+JyFlMxOPxJW4SWZ5JKRZZO+y48jYRF8a3n9w/2PHurkl9fGZ5Be3h0/MOEwiod33vxlk5CuByZCBCkzSLKLILFPH2QonIpTupl+tQZxY/DWPtyNn1aaFcxCr9iToD4T1v08/WqouF88zRjaNIJgBl+hk1I8u3aCK7xFqA1EffosQwL5Gp0/9u6s+kTPWw5yAJsEM6WWxG/Ika4bK4JnzCosi0CzHZ/VWuT2KTSKA7Q/a6KttZFk1kpWVtpV+eH+YlMn/PU+aJRJa0SOEPcsPQQ17T24t4BlMeyiffqEXhIvMgqDcp5I9n+lgXaSoHWXUe1jWHWM9LWZ6+SCKz+Eblh3HPyJOKDPOSeSyRJa9LxsTqwYr0GA+kkY9YF9vljHJxTfmUx+tV3eKLD+4dyM/rOy0CbxexLn2cFImY7WDCJz5KuEgiX7AxbcoPV1Jk8DbDsLaVUiwysiIScjTtqqX4guAXPR70YucYOKE8EluLwuHascc2froGylR+YuJuAHHiXES+k09pfI/t9HTwRUIs6WqDyojlKC6W5THcIcaN8deMsS9Au7y/wLHM41Sejx191HWP9XjwcmOstxuaxqCUYpGRyHfQWYCcW1cGBzLCQGd5M5i4ODHDcMFEzM/fwHHDrwODH/PHczSoD9QX0yJeVpzsSWI8nXbwI05sY+wzEFfSXvVNZP13fMyb2t50hxrGzB72JsVvaxlxoEbBim4aIME76UxiyCY1gzqy/NQfY9WHqyHysPGgXbQpS78SIkNW9zUpMg8kdAb4OZqfpZ8/trX/E/VHb+5I842CAaO8B/Zdfl+8tLSpc+rZ1aH/9QYMIm3hEwn0toLbKZ+kZe+2HdKVR2X1rv8n1TC8HEk2bQxQt7fJY7Iy1P94PRKla2qPoEyPB425oIwYM4qrLnITnHk5j/sDHtf0YAkcTXR2BtKAwfK85CMuq6fSDhZWZIGAiOhiN4G0gMgSPiuz0j4WXuRKpYQqcqUVVJErraCKXGkFVeRKK6giV1pBFbnSCqrIlVZQRa60gPXOv8eAOMDhI4WAAAAAAElFTkSuQmCC\\\">";
        const DOMAIN_DATA = ${JSON.stringify(data)};
    </script>
</html>`;
    stream.end(html);
    });
})


